package data.LocalDAO;

import data.DataAccessContext;
import data.DataAccessProvider;

/**
 * Created by: Pieter on 19/06/2015.
 */
public class LocalProvider implements DataAccessProvider {
    public DataAccessContext getDataAccessContext(){
        return new LocalDAC();
    }
}
