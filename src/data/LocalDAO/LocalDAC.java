package data.LocalDAO;

import data.DataAccessContext;
import data.DayDAO;

/**
 * Created by: Pieter on 19/06/2015.
 */
public class LocalDAC implements DataAccessContext {
    public DayDAO getDayDAO(){
        return new LocalDayDAO();
    }
}
