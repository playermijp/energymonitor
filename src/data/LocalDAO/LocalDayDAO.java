package data.LocalDAO;

import data.DataAccessException;
import data.Day;
import data.DayDAO;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by: Pieter on 19/06/2015.
 */
public class LocalDayDAO implements DayDAO {
    private final String JDBC_URL = "jdbc:sqlite:save.em2";

    @Override
    public Day createDay(int dayCounter, int nightCounter, int gasCounter, int solarEnergy, LocalDate date) throws DataAccessException {
        try (Connection conn = DriverManager.getConnection(JDBC_URL)){
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO days (Date, DayElectricity, NightElectricity, SolarEnergy, GasUsage)" +
                    " VALUES (?, ?, ?, ?, ?)");
            stmt.setString(1, date.toString());
            stmt.setInt(2, dayCounter);
            stmt.setInt(3, nightCounter);
            stmt.setInt(4, solarEnergy);
            stmt.setInt(5, gasCounter);
            stmt.executeUpdate();
            stmt.close();
            return new Day(dayCounter, nightCounter, gasCounter, solarEnergy, date);
        } catch (SQLException e){
            throw new DataAccessException(e);
        }
    }

    @Override
    public void setDayCounter(Day day, int dayCounter) throws DataAccessException {
        updateValue(day, dayCounter, "DayElectricity");
    }

    @Override
    public void setNightCounter(Day day, int nightCounter) throws DataAccessException {
        updateValue(day, nightCounter, "NightElectricity");
    }

    @Override
    public void setSolarCounter(Day day, int solarCounter) throws DataAccessException {
        updateValue(day, solarCounter, "SolarEnergy");
    }

    @Override
    public void setGasCounter(Day day, int gasCounter) throws DataAccessException {
        updateValue(day, gasCounter, "GasUsage");
    }

    private void updateValue(Day day, int value, String criteria) throws DataAccessException{
        try (Connection conn = DriverManager.getConnection(JDBC_URL)){
            PreparedStatement stmt = conn.prepareStatement("UPDATE days SET " + criteria + " = ? WHERE Date = ?");
            stmt.setInt(1, value);
            stmt.setString(2, day.getDate().toString());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e){
            throw new DataAccessException(e);
        }
    }

    @Override
    public void removeDay(Day day) throws DataAccessException {
        try (Connection conn = DriverManager.getConnection(JDBC_URL)){
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM days WHERE Date = ?");
            stmt.setString(1, day.getDate().toString());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e){
            throw new DataAccessException(e);
        }
    }

    @Override
    public List<Day> filter(Filter... filters) throws DataAccessException {
        String base = "SELECT * FROM days";
        if (filters.length > 0){
            base += " WHERE ";
            base += ((LocalFilter) filters[0]).getSqlStatement();
            for (int i = 1; i < filters.length; i++){
                base += " AND " + ((LocalFilter) filters[i]).getSqlStatement();
            }
        }

        try (Connection conn = DriverManager.getConnection(JDBC_URL)){
            PreparedStatement stmt = conn.prepareStatement(base);
            int index = 1;
            for (Filter filter: filters){
                ((LocalFilter) filter).setValue(stmt, index);
                index++;
            }
            ResultSet result = stmt.executeQuery();
            List<Day> output = new ArrayList<>();
            while(result.next()){
                LocalDate date = LocalDate.parse(result.getString("Date"));
                int dayCounter = result.getInt("DayElectricity");
                int nightCounter = result.getInt("NightElectricity");
                int solarEnergy = result.getInt("SolarEnergy");
                int gasUsage = result.getInt("GasUsage");
                output.add(new Day(dayCounter, nightCounter, gasUsage, solarEnergy, date));
            }
            return output;
        } catch (SQLException e){
            throw new DataAccessException(e);
        }
    }

    @Override
    public Filter filterOnDayCounter(int dayCounter) {
        return new LocalFilter("DayElectricity = ?", String.valueOf(dayCounter));
    }

    @Override
    public Filter filterOnNightCounter(int nightCounter) {
        return new LocalFilter("NightElectricity = ?", String.valueOf(nightCounter));
    }

    @Override
    public Filter filterOnSolarCounter(int solarCounter) {
        return new LocalFilter("SolarEnergy = ?", String.valueOf(solarCounter));
    }

    @Override
    public Filter filterOnGasCounter(int gasCounter) {
        return new LocalFilter("GasUsage = ?", String.valueOf(gasCounter));
    }

    @Override
    public Filter filterOnDate(LocalDate date) {
        return new LocalFilter("Date = ?", date.toString());
    }

    @Override
    public Filter filterOnYear(int year){
        return new LikeFilter("Date LIKE ?", String.valueOf(year));
    }

    @Override
    public Filter filterOnMonth(int month, int year) {
        return new LikeFilter("Date LIKE ?", String.valueOf(year + "-" + String.format("%02d", month)));
    }

    public class LocalFilter implements Filter {
        private String value;
        private String sqlStatement;

        public LocalFilter(String sqlStatement, String value){
            this.value = value;
            this.sqlStatement = sqlStatement;
        }

        public String getSqlStatement(){
            return sqlStatement;
        }

        public void setValue(PreparedStatement stmt, int index) throws SQLException {
            stmt.setString(index, value);
        }
    }

    public class LikeFilter extends LocalFilter {
        private String value;

        public LikeFilter(String sqlStatement, String value){
            super(sqlStatement, value);
            this.value = value;
        }

        @Override
        public void setValue(PreparedStatement stmt, int index) throws SQLException {
            stmt.setString(index, value + "%");
        }
    }

}
