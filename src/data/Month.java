package data;

import data.Day;
import data.TimePeriod;

import java.time.format.TextStyle;
import java.util.Locale;

/**
 * Created by: Pieter on 21/06/2015.
 */
public class Month implements TimePeriod {
    private Day first;
    private Day last;

    public Month(Day first, Day last){
        if (first.getDate().isAfter(last.getDate())){
            throw new IllegalArgumentException("First day must fall before last day!");
        }
        this.first = first;
        this.last = last;
    }

    @Override
    public int getDayElectricity() {
        return last.getDayCounter() - first.getDayCounter();
    }

    @Override
    public int getNightElectricity() {
        return last.getNightCounter() - first.getNightCounter();
    }

    @Override
    public int getSolarElectricity() {
        return last.getSolarEnergy() - first.getSolarEnergy();
    }

    @Override
    public int getGasUsage() {
        return last.getGasCounter() - first.getGasCounter();
    }

    @Override
    public String getPeriodName() {
        return last.getDate().getMonth().getDisplayName(TextStyle.FULL, Locale.getDefault()) + " " + last.getDate().getYear();
    }
}
