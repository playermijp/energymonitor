package data;

/**
 * Created by: Pieter on 21/06/2015.
 */
public interface TimePeriod {
    public int getDayElectricity();
    public int getNightElectricity();
    public int getSolarElectricity();
    public int getGasUsage();
    public String getPeriodName();
}
