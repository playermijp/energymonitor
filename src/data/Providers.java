package data;

import data.LocalDAO.LocalProvider;

/**
 * Created by: Pieter on 19/06/2015.
 */
public class Providers {
    public static DataAccessProvider getLocalProvider(){
        return new LocalProvider();
    }
}
