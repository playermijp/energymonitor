package data;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

/**
 * Created by: Pieter on 19/06/2015.
 */
public interface DayDAO {
    public Day createDay(int dayCounter, int nightCounter, int gasCounter, int solarEnergy, LocalDate date) throws DataAccessException;
    public void setDayCounter(Day day, int dayCounter) throws DataAccessException;
    public void setNightCounter(Day day, int nightCounter) throws DataAccessException;
    public void setSolarCounter(Day day, int solarCounter) throws DataAccessException;
    public void setGasCounter(Day day, int gasCounter) throws DataAccessException;
    public void removeDay(Day day) throws DataAccessException;
    public List<Day> filter(Filter ...filters) throws DataAccessException;

    public interface Filter {

    }

    public Filter filterOnDayCounter(int dayCounter);
    public Filter filterOnNightCounter(int nightCounter);
    public Filter filterOnSolarCounter(int solarCounter);
    public Filter filterOnGasCounter(int gasCounter);
    public Filter filterOnDate(LocalDate date);
    public Filter filterOnYear(int year);
    public Filter filterOnMonth(int month, int year);
}
