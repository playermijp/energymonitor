package data;

import java.util.List;

/**
 * Created by: Pieter on 20/06/2015.
 */
public class Year implements TimePeriod {
    private List<Day> days;

    public Year(List<Day> days){
        this.days = days;
        days.sort((one, two) -> one.getDate().compareTo(two.getDate()));
    }

    public int getDayElectricity(){
        if (days.size() > 0) {
            return days.get(days.size() - 1).getDayCounter() - days.get(0).getDayCounter();
        } else {
            return 0;
        }
    }

    public int getNightElectricity(){
        if (days.size() > 0) {
            return days.get(days.size() - 1).getNightCounter() - days.get(0).getNightCounter();
        } else {
            return 0;
        }
    }

    public int getSolarElectricity(){
        if (days.size() > 0) {
            return days.get(days.size() - 1).getSolarEnergy() - days.get(0).getSolarEnergy();
        } else {
            return 0;
        }
    }

    public int getGasUsage(){
        if (days.size() > 0) {
            return days.get(days.size() - 1).getGasCounter() - days.get(0).getGasCounter();
        } else {
            return 0;
        }
    }

    public String getPeriodName(){
        if (days.size() > 0) {
            return String.valueOf(days.get(0).getDate().getYear());
        } else {
            return "0";
        }
    }
}
