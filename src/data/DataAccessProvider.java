package data;

/**
 * Created by: Pieter on 19/06/2015.
 */
public interface DataAccessProvider {
    public DataAccessContext getDataAccessContext();
}
