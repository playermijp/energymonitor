package data;

/**
 * Created by: Pieter on 19/06/2015.
 */
public interface DataAccessContext {
    public DayDAO getDayDAO();
}
