package data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by: Pieter on 19/06/2015.
 */
public class Day {
    private int dayCounter;
    private int nightCounter;
    private int gasCounter;
    private int solarEnergy;
    private LocalDate date;

    public Day(int dayCounter, int nightCounter, int gasCounter, int solarEnergy, LocalDate date){
        this.dayCounter = dayCounter;
        this.nightCounter = nightCounter;
        this.gasCounter = gasCounter;
        this.solarEnergy = solarEnergy;
        this.date = date;
    }

    public int getDayCounter() {
        return dayCounter;
    }

    public void setDayCounter(int dayCounter) {
        this.dayCounter = dayCounter;
    }

    public int getNightCounter() {
        return nightCounter;
    }

    public void setNightCounter(int nightCounter) {
        this.nightCounter = nightCounter;
    }

    public int getGasCounter() {
        return gasCounter;
    }

    public void setGasCounter(int gasCounter) {
        this.gasCounter = gasCounter;
    }

    public int getSolarEnergy() {
        return solarEnergy;
    }

    public void setSolarEnergy(int solarEnergy) {
        this.solarEnergy = solarEnergy;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDateString(){
        return date.toString();
    }
}
