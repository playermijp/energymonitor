package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class Main extends Application {
    public static final String JDBC_DRIVER = "org.sqlite.JDBC";
    public static final String JDBC_URL = "jdbc:sqlite:save.em2";

    private void showJDBCNotFoundDialog(Exception ex){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Database driver not found!");
        alert.setHeaderText("Database driver not found!");
        alert.setContentText("Energy Monitor was unable to load necessary database drivers. Please try again!");

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        String sql = "CREATE TABLE days" +
                "(" +
                "  Date VARCHAR(255) NOT NULL," +
                "  DayElectricity int," +
                "  NightElectricity int," +
                "  SolarEnergy int," +
                "  GasUsage int" +
                ")";

        Parent root = FXMLLoader.load(getClass().getResource("gui.fxml"), ResourceBundle.getBundle("i18n"));
        primaryStage.setTitle("Energy Monitor");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/resources/icon.png")));
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException e){
            showJDBCNotFoundDialog(e);
        }

        //Check if there exists a save-file
        File f = new File("save.em2");
        if(!f.exists() || f.isDirectory()) {
            System.out.println("New file created!");
            try (Connection conn = DriverManager.getConnection(JDBC_URL)){
                Statement stmt = conn.createStatement();
                //Create a new table if the file doesn't exist
                stmt.executeUpdate(sql);
                stmt.close();
            } catch (SQLException e){
                showJDBCNotFoundDialog(e);
            }
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
