package gui.components;

import data.DataAccessException;
import data.Day;
import data.DayDAO;
import data.Providers;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.IntegerStringConverter;

/**
 * Created by: Pieter on 21/06/2015.
 */
public class MainTableController {
    public TableView<Day> table;
    public TableColumn<Day, String> dateColumn;
    public TableColumn<Day, Integer> dayElectricityColumn;
    public TableColumn<Day, Integer> nightElectricityColumn;
    public TableColumn<Day, Integer> solarEnergyColumn;
    public TableColumn<Day, Integer> gasUsageColumn;

    private DayDAO dayDAO;

    private ObservableList<Day> model;
    public MainTableController(){
        this.model = FXCollections.observableArrayList();
        this.dayDAO = Providers.getLocalProvider().getDataAccessContext().getDayDAO();
    }

    public void initialize(){
        table.setItems(model);

        try {
            model.addAll(dayDAO.filter());
        } catch (DataAccessException e){
            System.err.println("An error occurred: " + e);
        }

        table.setEditable(true);

        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        dayElectricityColumn.setCellValueFactory(new PropertyValueFactory<>("dayCounter"));
        nightElectricityColumn.setCellValueFactory(new PropertyValueFactory<>("nightCounter"));
        solarEnergyColumn.setCellValueFactory(new PropertyValueFactory<>("solarEnergy"));
        gasUsageColumn.setCellValueFactory(new PropertyValueFactory<>("gasCounter"));

        dateColumn.setEditable(false);
        //Table is sorted by date by default
        dateColumn.setSortType(TableColumn.SortType.DESCENDING);
        table.getSortOrder().add(dateColumn);

        dayElectricityColumn.setCellFactory(TextFieldTableCell.<Day, Integer>forTableColumn(new IntegerStringConverter()));
        nightElectricityColumn.setCellFactory(TextFieldTableCell.<Day, Integer>forTableColumn(new IntegerStringConverter()));
        solarEnergyColumn.setCellFactory(TextFieldTableCell.<Day, Integer>forTableColumn(new IntegerStringConverter()));
        gasUsageColumn.setCellFactory(TextFieldTableCell.<Day, Integer>forTableColumn(new IntegerStringConverter()));

        dayElectricityColumn.setOnEditCommit(e -> {
            try {
                dayDAO.setDayCounter(e.getRowValue(), e.getNewValue());
                refresh();
            } catch (DataAccessException exc){
                System.err.println("An error occurred: " + e);
            }
        });

        nightElectricityColumn.setOnEditCommit(e -> {
            try {
                dayDAO.setNightCounter(e.getRowValue(), e.getNewValue());
                refresh();
            } catch (DataAccessException exc){
                System.err.println("An error occurred: " + e);
            }
        });

        solarEnergyColumn.setOnEditCommit(e -> {
            try {
                dayDAO.setSolarCounter(e.getRowValue(), e.getNewValue());
                refresh();
            } catch (DataAccessException exc){
                System.err.println("An error occurred: " + e);
            }
        });

        gasUsageColumn.setOnEditCommit(e -> {
            try {
                dayDAO.setGasCounter(e.getRowValue(), e.getNewValue());
                refresh();
            } catch (DataAccessException exc){
                System.err.println("An error occurred: " + e);
            }
        });
    }

    private void refresh(){
        try {
            model.clear();
            model.addAll(dayDAO.filter());
        } catch (DataAccessException e){
            System.err.println("An error occurred: " + e);
        }
    }

    public void removeDay(Day remove){
        try {
            dayDAO.removeDay(remove);
            model.remove(remove);
        } catch (DataAccessException e){
            System.err.println("An error occurred: " + e);
        }
    }

    public ObservableList<Day> getModel(){
        return model;
    }
}
