package gui.components;

import data.Day;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableView;

import java.io.IOException;
import java.util.ResourceBundle;

/**
 * Created by: Pieter on 21/06/2015.
 */
public class MainTable extends TableView<Day> {
    MainTableController controller;

    public MainTable(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/gui/components/MainTable.fxml"), ResourceBundle.getBundle("i18n"));
            controller = new MainTableController();
            loader.setController(controller);
            loader.setRoot(this);
            loader.load();
        } catch (IOException e){
            throw new RuntimeException("An error occurred while loading the MainTable", e);
        }
    }


    public void removeDay(Day remove){
        controller.removeDay(remove);
    }

    public ObservableList<Day> getModel(){
        return controller.getModel();
    }
}
