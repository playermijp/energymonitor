package gui.components;

import data.TimePeriod;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by: Pieter on 21/06/2015.
 */
public class TimePeriodGraph extends VBox {
    private TimePeriodGraphController controller;
    private String dayCounter = "Dagteller";
    private String nightCounter = "Nachtteller";
    private String solarCounter = "Solarteller";
    private String gasCounter = "Gasteller";


    public TimePeriodGraph(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/gui/components/TimePeriodGraph.fxml"), ResourceBundle.getBundle("i18n"));
            controller = new TimePeriodGraphController();
            loader.setController(controller);
            loader.setRoot(this);
            loader.load();
        } catch (IOException e){
            throw new RuntimeException("An error occurred while loading a TimePeriodGraph", e);
        }
    }

    public void update(List<TimePeriod> periods){
        controller.graph.getData().clear();
        controller.graph.getYAxis().setLabel("Kwh / m3");
        controller.graph.setAnimated(false);

        for (TimePeriod period: periods) {
            XYChart.Series<String, Number> series = new XYChart.Series<>();
            series.setName(String.valueOf(period.getPeriodName()));
            series.getData().add(new XYChart.Data<>(dayCounter, period.getDayElectricity()));
            series.getData().add(new XYChart.Data<>(nightCounter, period.getNightElectricity()));
            series.getData().add(new XYChart.Data<>(solarCounter, period.getSolarElectricity()));
            series.getData().add(new XYChart.Data<>(gasCounter, period.getGasUsage()));
            controller.graph.getData().add(series);
        }
    }

    public void addData(List<TimePeriod> periods){
        for (TimePeriod period: periods) {
            XYChart.Series<String, Number> series = new XYChart.Series<>();
            series.setName(String.valueOf(period.getPeriodName()));
            series.getData().add(new XYChart.Data<>(dayCounter, period.getDayElectricity()));
            series.getData().add(new XYChart.Data<>(nightCounter, period.getNightElectricity()));
            series.getData().add(new XYChart.Data<>(solarCounter, period.getSolarElectricity()));
            series.getData().add(new XYChart.Data<>(gasCounter, period.getGasUsage()));
            controller.graph.getData().add(series);
        }
    }

    public void setTitle(String title){
        controller.graph.setTitle(title);
    }

    public String getTitle(){
        return controller.graph.getTitle();
    }

    public void reset(){
        controller.graph.getData().clear();
    }
}
