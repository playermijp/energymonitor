package gui.components;

import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;

/**
 * Created by: Pieter on 21/06/2015.
 */
public class TimePeriodGraphController {
    private CategoryAxis xAxis = new CategoryAxis();
    private NumberAxis yAxis = new NumberAxis();
    public BarChart<String, Number> graph = new BarChart<>(xAxis, yAxis);
}
