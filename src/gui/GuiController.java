package gui;

import data.*;
import data.Month;
import gui.components.MainTable;
import gui.components.TimePeriodGraph;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class GuiController {
    public MainTable table;

    public Spinner<Integer> yearSpinner;
    public TimePeriodGraph yearGraph;

    public Spinner<Integer> monthSpinner;
    public Spinner<Integer> monthYearSpinner;
    public TimePeriodGraph monthGraph;

    private DayDAO dayDAO;
    private List<TimePeriod> monthList = new ArrayList<>();

    public void initialize() throws DataAccessException {
        dayDAO = Providers.getLocalProvider().getDataAccessContext().getDayDAO();

        yearSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 15, 3));
        yearSpinner.valueProperty().addListener(o -> yearChart());

        monthSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 12, LocalDate.now().getMonthValue()));
        monthYearSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1950, 2100, LocalDate.now().getYear()));

        monthChart();
        yearChart();
    }

    public void close(){
        ((Stage) yearSpinner.getScene().getWindow()).close();
    }

    public void about(){
        try {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("/gui/About.fxml"), ResourceBundle.getBundle("i18n"));
            stage.setTitle("Energy Monitor");
            stage.getIcons().add(new Image(getClass().getResourceAsStream("/resources/icon.png")));
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.show();
        } catch (IOException e){
            System.err.println("An error occurred: " + e);
        }
    }

    public void addData(){
        try {
            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/gui/AddData.fxml"), ResourceBundle.getBundle("i18n"));
            loader.setController(new AddController(table.getModel()));
            Parent root = loader.load();
            stage.setTitle("Energy Monitor");
            stage.getIcons().add(new Image(getClass().getResourceAsStream("/resources/icon.png")));
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e){
            System.err.println("An error occurred: " + e);
        }
    }

    public void deleteSelected(){
        table.removeDay(table.getSelectionModel().getSelectedItem());
    }

    public void yearChart(){
        try {
            List<TimePeriod> yearList = new ArrayList<>();

            for (int i = yearSpinner.getValue() - 1; i >= 0; i--){
                yearList.add(new Year(dayDAO.filter(dayDAO.filterOnYear(LocalDate.now().getYear() - i))));
            }

            yearGraph.update(yearList);

        } catch (DataAccessException e){
            System.err.println("An error occurred: " +  e);
        }
    }

    public void monthChart(){
        monthGraph.update(monthList);
    }

    public void addMonth(){
        try {
            List<Day> one = dayDAO.filter(dayDAO.filterOnMonth(monthSpinner.getValue(), monthYearSpinner.getValue()));
            List<Day> two = null;
            if (monthSpinner.getValue() > 1){
                two = dayDAO.filter(dayDAO.filterOnMonth(monthSpinner.getValue() - 1, monthYearSpinner.getValue()));
            } else {
                two = dayDAO.filter(dayDAO.filterOnMonth(12, monthYearSpinner.getValue() - 1));
            }
            if (one.size() > 0 && two.size() > 0){
                one.sort((first, second) -> first.getDate().compareTo(second.getDate()));
                two.sort((first, second) -> second.getDate().compareTo(first.getDate()));
                monthList.add(new Month(two.get(0) ,one.get(one.size() - 1)));
                monthGraph.update(monthList);
            }
        } catch (DataAccessException e){
            System.err.println("An error occurred: " + e);
        }
    }

    public void resetMonthChart(){
        monthList.clear();
        monthGraph.reset();
    }
}
