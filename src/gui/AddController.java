package gui;

import data.DataAccessException;
import data.Day;
import data.DayDAO;
import data.Providers;
import javafx.collections.ObservableList;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.time.LocalDate;


/**
 * Created by: Pieter on 19/06/2015.
 */
public class AddController {
    public DatePicker datePicker;
    public TextField dayCounterField;
    public TextField nightCounterField;
    public TextField solarCounterField;
    public TextField gasCounterField;

    private DayDAO dayDAO;
    private ObservableList<Day> model;

    public AddController(ObservableList<Day> model){
        this.model = model;
    }

    public void initialize(){
        dayDAO = Providers.getLocalProvider().getDataAccessContext().getDayDAO();
    }

    public void send(){
        try {
            int dayCounter = Integer.parseInt(dayCounterField.getText());
            int nightCounter = Integer.parseInt(nightCounterField.getText());
            int solarCounter = Integer.parseInt(solarCounterField.getText());
            int gasCounter = Integer.parseInt(gasCounterField.getText());
            LocalDate date = datePicker.getValue();
            model.add(dayDAO.createDay(dayCounter, nightCounter, gasCounter, solarCounter, date));
            ((Stage) gasCounterField.getScene().getWindow()).close();
        } catch (DataAccessException e){
            System.err.println("An error occurred: " + e);
        } catch (NumberFormatException e){
            System.err.println("An error occurred: " + e);
        }
    }
}
