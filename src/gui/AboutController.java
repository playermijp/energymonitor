package gui;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by: Pieter on 19/06/2015.
 */
public class AboutController {
    public Label titleLabel;
    public Label developerLabel;
    public Label versionLabel;
    public ImageView iconView;

    public void initialize(){
        try {
            Properties prop = new Properties();
            prop.load(getClass().getResourceAsStream("/resources/data.properties"));
            titleLabel.setText(prop.getProperty("title", "Energy Monitor"));
            developerLabel.setText(prop.getProperty("developer", "ABYX"));
            versionLabel.setText(prop.getProperty("version"));
            iconView.setImage(new Image(getClass().getResourceAsStream("/resources/icon.png")));
        } catch (IOException e){
            System.err.println("An error occured: " + e);
        }
    }

    public void close(){
        ((Stage) iconView.getScene().getWindow()).close();
    }
}
