/**
 * Created by: Pieter on 19/06/2015.
 */
public class Prepare {
    String sql = "CREATE TABLE days" +
            "(" +
            "  Date VARCHAR(255) NOT NULL," +
            "  DayElectricity int," +
            "  NightElectricity int," +
            "  SolarEnergy int," +
            "  GasUsage int" +
            ")";
}
